shake-js
========

This is a simple javascript which enables to 'shake' html elements



Pretty easy to use: `$$.shake('Element_id');`

Customizable: `$$.shake('Element_id', shake_distance, shake_delay, no_of_shakes);`


License
========

The source code will be provided to all of you with a [DBAD (Don't Be A Dick!)](https://github.com/philsturgeon/dbad/blob/master/LICENSE-en.md) licnese by [Phil Sturgeon](https://github.com/philsturgeon)

Happy Coding!
